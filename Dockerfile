FROM node:10

# where app lives inside the container
WORKDIR /usr/src/app

#to install dependencies on docker system
COPY package*.json ./

RUN npm install

COPY . .

# expose the port set in app.js
EXPOSE 3000

#how to run the app - use nmp and start script defined in package.json
CMD ["npm", "start"]
