const mongoose = require('mongoose');

//Refill schema
let refillSchema = mongoose.Schema({
    location: {
        type: String,
        required: true
    },
    amount: {
        type: Number,
        required: true
    },
    cost: {
        type: Number,
        required: true
    }

});

//parms: model_name, schema
let Refill = module.exports = mongoose.model('Refill', refillSchema);
