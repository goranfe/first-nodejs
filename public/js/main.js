$(document).ready(function() {
    $('.delete-refill').on('click', function(e) {
        $target = $(e.target);
        const id = $target.attr('data-id');
        $.ajax({
            type: 'DELETE',
            url: '/refills/'+id,
            success: function(response) {
                alert('Deleting refill');
                window.location.href='/refills';
            },
            error: function(err) {
                console.log(err);
            }
        });
    });
});
