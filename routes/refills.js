const express = require('express');
const router = express.Router();

//bring in Refill model
const Refill = require('../models/refill');


router.get('/add', (req, res) => {
    res.render('add_refill', {heading: 'Fuel refill', message: 'Ad new fuel refill'});
});

router.get('/edit/:id', (req, res) => {
    Refill.findById(req.params.id, (err, refill) => {
        res.render('edit_refill', {
            refill:refill
        });
    });
});

router.post('/edit/:id', (req, res) => {
    let refill = {};
    refill.location = req.body.location;
    refill.amount = req.body.amount;
    refill.cost = req.body.cost;

    let query = {_id:req.params.id}

    Refill.update(query, refill, err => {
        if (err) {
            console.log(err);
            return;
        } else {
            req.flash('success', 'Article Updated');
            res.redirect('/refills');
        }
    });
});

router.delete('/:id', (req, res) => {
    let query = {_id:req.params.id}
    Refill.remove(query, err => {
        if(err) {
            console.log(err);
        }
        res.send('Succes');  // status 200 OK by default
    });
});

router.post('/add', (req, res) => {
    req.checkBody('location', 'Location is required').notEmpty();
    req.checkBody('amount', 'Amount is required').notEmpty();
    req.checkBody('cost', 'Cost is required').notEmpty();

    let errors = req.validationErrors();

    if (errors) {
        res.render('add_refill', {
            heading: 'Fuel refill',
            errors:errors
        });
    } else {
        let refill = new Refill();
        refill.location = req.body.location;
        refill.amount = req.body.amount;
        refill.cost = req.body.cost;

        refill.save( err => {
            if (err) {
                console.log(err);
                return;
            } else {
                req.flash('success', 'Article Added');
                res.redirect('/refills');
            }
        });
    }
});

// put last because it is a placeholder - can match anything
router.get('/:id', (req, res) => {
    Refill.findById(req.params.id, (err, refill) => {
        res.render('refill', {
            refill:refill
        });
    });
});

module.exports = router;
