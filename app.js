const path = require('path');
const express = require('express');
const mongoose = require('mongoose');
const passport = require('passport');
const flash = require('connect-flash');
const bodyParser = require('body-parser')
const session = require('express-session');
const config = require('./config/database');
const expressValidator = require('express-validator');

const app = express();
const port = 3000;

//MongoDb connection
mongoose.connect(
    config.database, // map to container_name (mongo)
    {useNewUrlParser: true});
let db = mongoose.connection;

//check connection
db.once('open', function() {
    console.log('connected to mongodb');
});

//check for db errors
db.on('error', function(err) {
    console.log(err);
});

//bring in models
const Refill = require('./models/refill');


//set PUG templates
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// Body Parser - parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());  // parse application/json

//set public folder
app.use(express.static(path.join(__dirname, 'public')));

//express session middleware
app.use(session({
  secret: 'keyboard cat',
  resave: true,
  saveUninitialized: true,
}));

//express messages middleware
app.use(require('connect-flash')());
app.use(function (req, res, next) {
  res.locals.messages = require('express-messages')(req, res);
  next();
});

//express validator middleware
app.use(expressValidator({
    errorFormater: function(param, msg, value) {
        var namespace = param.split('.')
        , root = namespace.shift()
        , formParam = root;

        while(namespace.length) {
            formParam += '[' + namespace.shift() + ']';
        }
        return {
            param : formParam,
            msg : msg,
            value : value
        };
    }
}));

//Passport config
require('./config/passport')(passport);
//Passport middleware
app.use(passport.initialize());
app.use(passport.session());

app.get('*', (req, res, next) => {
    res.locals.user = req.user || null;
    next();
});

///////////////////////////
//routes
app.get('/', (req, res) => {
    res.render('index', {heading: 'Vehicle cost logger', message: 'Hello'});
});

app.get('/refills', (req, res) => {
    Refill.find({}, function(err, refills) { // empty query - {}, response = refills
        if (err) {
            console.log(err);
        } else {
            res.render('refills', {
                heading: 'My refills',
                refills: refills
            });
        }
    });

});

// route files
let refills = require('./routes/refills.js');
let users = require('./routes/users.js');
app.use('/refills', refills);
app.use('/users', users);


//start app
app.listen(port, () => {
    console.log(`Server started on port ${port}`);
});
